function reverseNumber(num) {
  let str = num + '',
    resultStr = '',
    firstIndex;
  if (str[0] === '-') {
    str += '-';
    firstIndex = 1;
  } else {
    firstIndex = 0;
  }
  for (let i = str.length - 1; i >= firstIndex; i--) {
    resultStr += str[i];
  }
  return +resultStr;
}
// console.log(reverseNumber(-12345) + 5);
function forEach(arr, func) {
  for (let i = 0; i < arr.length; i++) {
    func(arr[i]);
  }
}
// forEach([2, 5, 8], function (val) {
//   console.log(val);
// });
function map(arr, func) {
  const resultArr = [];
  for (let i = 0; i < arr.length; i++) {
    resultArr.push(func(arr[i]));
  }
  return resultArr;
}
// console.log(
//   map([1, 2, 3, 4, 5], function (el) {
//     return el * 2;
//   })
// );
function filter(arr, func) {
  const resultArr = [];
  for (let i = 0; i < arr.length; i++) {
    if (func(arr[i])) {
      resultArr.push(arr[i]);
    }
  }
  return resultArr;
}
// console.log(
//   filter([1, 4, 6, 7, 8, 10], function (el) {
//     return el % 2 === 0;
//   })
// );
function getAdultAppleLovers(data) {
  return map(
    filter(data, function ({ age, favoriteFruit }) {
      return age > 18 && favoriteFruit === 'apple';
    }),
    ({ name }) => name
  );
}

// console.log(
//   getAdultAppleLovers([
//     {
//       _id: "5b5e3168c6bf40f2c1235cd6",
//       index: 0,
//       age: 39,
//       eyeColor: "green",
//       name: "Stein",

//       favoriteFruit: "apple",
//     },
//     {
//       _id: "5b5e3168e328c0d72e4f27d8",
//       index: 1,
//       age: 38,
//       eyeColor: "blue",
//       name: "Cortez",
//       favoriteFruit: "strawberry",
//     },
//     {
//       _id: "5b5e3168cc79132b631c666a",
//       index: 2,
//       age: 2,
//       eyeColor: "blue",
//       name: "Suzette",
//       favoriteFruit: "apple",
//     },
//     {
//       _id: "5b5e31682093adcc6cd0dde5",
//       index: 3,
//       age: 17,
//       eyeColor: "green",
//       name: "Weiss",
//       favoriteFruit: "banana",
//     },
//   ])
// );

function getKeys(obj) {
  const keys = [];
  for (const key in obj) {
    keys.push(key);
  }
  return keys;
}

// console.log(getKeys({ keyOne: 1, keyTwo: 2, keyThree: 3 }));

function getValues(obj) {
  const values = [];
  for (const key in obj) {
    values.push(obj[key]);
  }
  return values;
}
// console.log(getValues({ keyOne: 1, keyTwo: 2, keyThree: 3 }));
function showFormattedDate(dateObj) {
  return `It is ${dateObj.getDate()} of ${dateObj.toLocaleString('default', {
    month: 'short',
  })}, ${dateObj.getFullYear()}`;
}
// console.log(showFormattedDate(new Date("2018-08-27T01:10:00")));
