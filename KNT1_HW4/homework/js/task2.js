let gameMap = {
  1: 100,
  2: 50,
  3: 25,
};
let max = 8,
  earn = 0;

let startGame = confirm('Do you want to play a game?');
if (!startGame) {
  alert('You did not become a billionaire, but can.');
  throw new Error('Game declined');
}

do {
  const randomNum = Math.round(Math.random() * max);
  console.log(randomNum);
  let answered = false;
  for (const key in gameMap) {
    const userInput = +prompt(
      `Choose a roulette pocket number from 0 to ${max}\nAttempts left: ${
        3 - key + 1
      }\nTotal prize: ${earn}$\nPossible prize on current attempt: ${
        gameMap[key]
      }$`
    );
    if (userInput === randomNum) {
      answered = true;
      earn += gameMap[key];
      startGame = confirm(
        `Congratulation, you won! Your prize is: ${earn}$. Do you want to continue?`
      );
      if (startGame) {
        gameMap = {
          1: gameMap[1] * 2,
          2: gameMap[2] * 2,
          3: gameMap[3] * 2,
        };
        max += 4;
        break;
      } else {
        checkStartOver();
        break;
      }
    }
  }
  if (!answered) {
    checkStartOver();
  }
} while (startGame);
function checkStartOver() {
  startGame = confirm(
    `Thank you for your participation. Your prize is: ${earn}$. Do you want to start over?`
  );
  if (startGame) {
    reset();
  }
}
function reset() {
  gameMap = {
    1: 100,
    2: 50,
    3: 25,
  };
  max = 8;
  earn = 0;
}
