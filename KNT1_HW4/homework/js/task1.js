let initialDeposit, years, percentage;

initialDeposit = +prompt('Money: ');

if (!initialDeposit || initialDeposit < 1000) {
  alertInvalidInput('initial amount can’t be less than 1000');
}
years = +prompt('Years: ');
if (!years || years < 1) {
  alertInvalidInput('Number of years can`t be less than 1');
}
percentage = +prompt('Percentage: ');
if (!percentage || percentage > 100) {
  alertInvalidInput('Percentage can`t be bigger than 100');
}

alert(
  `Initial amount: ${initialDeposit}\nNumber of years: ${years}\nPercentage of year: ${percentage}
  `
);
let i = 1,
  yearProfit = initialDeposit / percentage;

let totalProfit = yearProfit,
  totalAmount = initialDeposit + yearProfit;

showYearIncome(1, yearProfit, totalAmount);
while (i < years) {
  totalProfit += totalAmount / percentage;
  totalAmount = initialDeposit + totalProfit;

  showYearIncome(++i, totalProfit, totalAmount);
}
function showYearIncome(year, totalProfit, totalAmount) {
  alert(
    `
Year ${year}
Total profit: ${totalProfit.toFixed(2)}
Total amount: ${totalAmount.toFixed(2)}
    `
  );
}
function alertInvalidInput(message) {
  alert('Invalid input data');
  throw new Error(message);
}
