function isEquals(first, second) {
  return first === second;
}
// console.log(isEquals(3, 3));

function isBigger(first, second) {
  return first > second;
}
// console.log(isBigger(-5, -1));

function storeNames(...names) {
  return names;
}
// console.log(storeNames("Tommy Shelby", "Ragnar Lodbrok", "Tom Hardy"));

function getDifference(first, second) {
  return first > second ? first - second : second - first;
}
// console.log(getDifference(1, 1));

function negativeCount(nums) {
  return nums.filter((num) => num < 0).length;
}
// console.log(negativeCount([0, -3, 5, 7]));

function letterCount(str, letter) {
  return str.split('').filter((strLetter) => strLetter === letter).length;
}
// console.log(letterCount("", "z"));

function countPoint(scores) {
  return scores.reduce((acc, score) => {
    const [ourTeamScore, enemyTeamScore] = score.split(':');
    if (+ourTeamScore > +enemyTeamScore) {
      return acc + 3;
    } else if (+ourTeamScore < +enemyTeamScore) {
      return acc;
    } else {
      return acc + 1;
    }
  }, 0);
}
console.log(
  countPoint([
    '100:90',
    '110:98',
    '100:100',
    '95:46',
    '54:90',
    '99:44',
    '90:90',
    '111:100',
  ])
);
